﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InvoiceManager.Migrations
{
    public partial class FixFKs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_Localities_LocalityID",
                table: "Clients");

            migrationBuilder.DropForeignKey(
                name: "FK_Invoices_Clients_ClientID",
                table: "Invoices");

            migrationBuilder.AlterColumn<int>(
                name: "ClientID",
                table: "Invoices",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LocalityID",
                table: "Clients",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_Localities_LocalityID",
                table: "Clients",
                column: "LocalityID",
                principalTable: "Localities",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Invoices_Clients_ClientID",
                table: "Invoices",
                column: "ClientID",
                principalTable: "Clients",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_Localities_LocalityID",
                table: "Clients");

            migrationBuilder.DropForeignKey(
                name: "FK_Invoices_Clients_ClientID",
                table: "Invoices");

            migrationBuilder.AlterColumn<int>(
                name: "ClientID",
                table: "Invoices",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "LocalityID",
                table: "Clients",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_Localities_LocalityID",
                table: "Clients",
                column: "LocalityID",
                principalTable: "Localities",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Invoices_Clients_ClientID",
                table: "Invoices",
                column: "ClientID",
                principalTable: "Clients",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}