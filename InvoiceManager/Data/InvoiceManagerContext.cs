﻿using InvoiceManager.Models;
using Microsoft.EntityFrameworkCore;

namespace InvoiceManager.Data
{
    public class InvoiceManagerContext : DbContext
    {
        public InvoiceManagerContext(DbContextOptions<InvoiceManagerContext> options) : base(options)
        {
        }

        public DbSet<Invoice> Invoices { get; set; }

        public DbSet<Client> Clients { get; set; }

        public DbSet<Article> Articles { get; set; }

        public DbSet<Locality> Localities { get; set; }

        public DbSet<ArticleInvoice> ArticleInvoices { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ArticleInvoice>()
                .HasKey(ai => new { ai.ArticleId, ai.InvoiceId });

            modelBuilder.Entity<ArticleInvoice>()
                .HasOne(ai => ai.Article)
                .WithMany(a => a.ArticleInvoices)
                .HasForeignKey(ai => ai.ArticleId);

            modelBuilder.Entity<ArticleInvoice>()
                .HasOne(ai => ai.Invoice)
                .WithMany(i => i.ArticleInvoices)
                .HasForeignKey(ai => ai.InvoiceId);
        }
    }
}