﻿using InvoiceManager.Data;
using InvoiceManager.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace InvoiceManager.Controllers
{
    public class InvoicesController : Controller
    {
        private readonly InvoiceManagerContext _context;

        public InvoicesController(InvoiceManagerContext context)
        {
            _context = context;
        }

        // GET: Invoices
        public async Task<IActionResult> Index()
        {
            var invoiceManagerContext = _context.Invoices.Include(i => i.Client);
            return View(await invoiceManagerContext.ToListAsync());
        }

        // GET: Invoices/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoice = await _context.Invoices
                .Include(i => i.Client)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (invoice == null)
            {
                return NotFound();
            }

            return View(invoice);
        }

        // GET: Invoices/Create
        public IActionResult Create()
        {
            ViewData["ClientID"] = new SelectList(_context.Clients, "ID", "FullName");
            ViewData["Articles"] = _context.Articles;
            return View();
        }

        // POST: Invoices/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Date,ClientID")] Invoice invoice, string ArticlesArray)
        {
            invoice.ArticleInvoices = new List<ArticleInvoice>();
            List<ArticleQuantity> articles = JsonConvert.DeserializeObject<List<ArticleQuantity>>(ArticlesArray ?? "[]");
            foreach (var a in articles.Where(a => a.Quantity > 0))
            {
                invoice.ArticleInvoices.Add(new ArticleInvoice()
                {
                    ArticleId = a.ArticleId,
                    Quantity = a.Quantity,
                    Invoice = invoice
                });
            }
            if (ModelState.IsValid)
            {
                _context.Add(invoice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientID"] = new SelectList(_context.Clients, "ID", "FullName", invoice.ClientID);
            ViewData["Articles"] = _context.Articles;
            return View(invoice);
        }

        // GET: Invoices/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoice = await _context.Invoices.FindAsync(id);
            if (invoice == null)
            {
                return NotFound();
            }
            ViewData["ClientID"] = new SelectList(_context.Clients, "ID", "FullName", invoice.ClientID);
            ViewData["Articles"] = _context.Articles;
            return View(invoice);
        }

        // POST: Invoices/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Date,ClientID")] Invoice invoice)
        {
            if (id != invoice.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(invoice);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InvoiceExists(invoice.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientID"] = new SelectList(_context.Clients, "ID", "FullName", invoice.ClientID);
            ViewData["Articles"] = _context.Articles;
            return View(invoice);
        }

        // GET: Invoices/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoice = await _context.Invoices
                .Include(i => i.Client)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (invoice == null)
            {
                return NotFound();
            }

            return View(invoice);
        }

        // POST: Invoices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var invoice = await _context.Invoices.FindAsync(id);
            _context.Invoices.Remove(invoice);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InvoiceExists(int id)
        {
            return _context.Invoices.Any(e => e.ID == id);
        }

        // GET: Invoices/Download/5
        public async Task<IActionResult> Download(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoice = await _context.Invoices.FindAsync(id);
            if (invoice == null)
            {
                return NotFound();
            }

            return View(invoice);
        }

        public async Task<ActionResult> DownloadXML(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoice = await _context.Invoices.FindAsync(id);
            if (invoice == null)
            {
                return NotFound();
            }

            var converter = new SingleArrayNodeXmlConverter
            {
                DeserializeRootElementName = "Invoice",
                WriteArrayAttribute = false,
                ArrayElementName = "Article"
            };
            var xml = JsonConvert.DeserializeObject<XmlDocument>(JsonConvert.SerializeObject(invoice), converter);

            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(xml.OuterXml);
            var output = new FileContentResult(bytes, "application/xml")
            {
                FileDownloadName = "invoice" + id + ".xml"
            };

            return output;
        }

        public async Task<ActionResult> DownloadJSON(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoice = await _context.Invoices.FindAsync(id);
            if (invoice == null)
            {
                return NotFound();
            }

            var json = JsonConvert.SerializeObject(invoice, Newtonsoft.Json.Formatting.Indented);

            var bytes = System.Text.Encoding.UTF8.GetBytes(json);
            var output = new FileContentResult(bytes, "application/json")
            {
                FileDownloadName = "invoice" + id + ".json"
            };

            return output;
        }

        private class ArticleQuantity
        {
            public int ArticleId { get; set; }
            public int Quantity { get; set; }
        }

        private class SingleArrayNodeXmlConverter : XmlNodeConverter
        {
            public string ArrayElementName { get; set; }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                var token = JObject.Load(reader);
                InjectArrayElements(token, ArrayElementName);
                var innerReader = token.CreateReader();
                innerReader.Read();
                return base.ReadJson(innerReader, objectType, existingValue, serializer);
            }

            private static void InjectArrayElements(JToken token, string elementName)
            {
                foreach (var childToken in token)
                {
                    InjectArrayElements(childToken, elementName);
                }
                if (token.Type == JTokenType.Array)
                {
                    var arrayHolder = new JObject { { elementName, token } };
                    token.Replace(arrayHolder);
                }
            }
        }
    }
}