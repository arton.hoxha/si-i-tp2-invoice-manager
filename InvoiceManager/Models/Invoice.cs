﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InvoiceManager.Models
{
    public class Invoice
    {
        [JsonIgnore]
        public int ID { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public double Total
        {
            get
            {
                var total = 0.0;
                if (ArticleInvoices != null)
                    foreach (var article in ArticleInvoices)
                    {
                        total += article.Article.Price * article.Quantity;
                    }
                return total;
            }
        }

        [JsonIgnore]
        public int ClientID { get; set; }

        public virtual Client Client { get; set; }

        [Display(Name = "Articles")]
        public virtual List<ArticleInvoice> ArticleInvoices { get; set; }
    }
}