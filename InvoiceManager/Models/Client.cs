﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InvoiceManager.Models
{
    public class Client
    {
        [JsonIgnore]
        public int ID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        [JsonIgnore]
        [Display(Name = "Client")]
        public string FullName => $"{FirstName} {LastName}, {CompanyName}";

        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }

        [JsonIgnore]
        public string Details => $"{Email}, {Address}, {Locality.ZIP} {Locality.Name}";

        [JsonIgnore]
        public int LocalityID { get; set; }

        [JsonIgnore]
        public virtual Locality Locality { get; set; }

        [JsonIgnore]
        public virtual List<Invoice> Invoices { get; set; }
    }
}