﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InvoiceManager.Models
{
    public class Locality
    {
        public int ID { get; set; }

        [Display(Name = "ZIP code")]
        public int ZIP { get; set; }

        public string Name { get; set; }

        [JsonIgnore]
        public virtual List<Client> Clients { get; set; }
    }
}