﻿using Newtonsoft.Json;

namespace InvoiceManager.Models
{
    public class ArticleInvoice
    {
        [JsonIgnore]
        public int ArticleId { get; set; }

        [JsonIgnore]
        public int InvoiceId { get; set; }

        public virtual Article Article { get; set; }

        [JsonIgnore]
        public virtual Invoice Invoice { get; set; }

        public int Quantity { get; set; }
    }
}