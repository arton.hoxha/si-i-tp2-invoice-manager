﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InvoiceManager.Models
{
    public class Article
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }

        [JsonIgnore]
        public virtual List<ArticleInvoice> ArticleInvoices { get; set; }
    }
}