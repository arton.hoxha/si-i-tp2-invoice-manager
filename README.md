# Invoice Manager v2
Invoice manager, but with a better framework behind it.

## Live Demo
The live demo is available on [Microsoft Azure](https://si-tp2-invoicemanager.azurewebsites.net/).

## Video
The video is available on [Microsoft Stream](https://web.microsoftstream.com/video/1062b095-8e8d-4457-a6e5-a077e10a2b95).

## Members
- Grinenko Artur
- Gamba Jérémy
- Hoxha Arton